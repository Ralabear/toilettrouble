Toilet Trouble
==============

You need a wee, but you don’t know where you can go! Try to go in bushes or
toilets however there are problems that may stop you from being able to relieve
yourself!


Running the Game
----------------
To run the windows version, launch the ToiletTrouble.exe file. For Linux and
Mac, install Pygame with `sudo apt install python-pygame` or similar. And then
run `python3 ToiletTrouble.py`

Dependencies (to run from source)
---------------------------------
- Pygame 1.9+
- Python 3


Credits
-------
The font used (Arimo) is licensed under an Apache 2 license which is contained
in the data directory.
This uses pygame which is licensed under the LGPL which is also in the data directory.
This project was made for the GMTK Game Jam 2020.