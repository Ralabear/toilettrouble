import pygame
import random

pygame.init()
displaysize = (1080, 720)
Name = "Toilet Trouble"

screen = pygame.display.set_mode(displaysize)
pygame.display.set_caption(Name)


def loadwithsize(name, size):
    earlypic = pygame.image.load(name).convert_alpha()
    return pygame.transform.smoothscale(earlypic, size)


mansize = (25, 40)
man = loadwithsize("Data/Man.png", mansize)
pygame.display.set_icon(man)
clock = pygame.time.Clock()


def round_pos(pos):
    return (round(pos[0]), round(pos[1]))


red = (250, 10, 10)

newrandom = True
collisionside = None
myfont = pygame.font.Font("Data/Arimo.ttf", 20)
menufont = pygame.font.Font("Data/Arimo.ttf", 70)
instructionsfont = pygame.font.Font("Data/Arimo.ttf", 44)
manwords = myfont.render("Watch out! The parkkeeper is watching", True, red)
rabbitwords = myfont.render("Don't wee on the rabbit!", True, red)
dogwords = myfont.render("Woof, Woof! Be careful or I'll nip your bits!", True, red)
queuewords = myfont.render("There is a long queue for this toilet!", True, red)
noentrywords = myfont.render("This toilet is out of order!", True, red)


instructs = """You need a wee, but you don’t know where you can
go! Use the arrow keys to move your player to places
where you go. However, there are things that will stop
you from being able to go so keep looking until you
are successful! After each failed attempt at relieving
yourself, your desperation level will increase!"""
instructs = [
    instructionsfont.render(i, True, (0, 0, 0)) for i in instructs.splitlines()
]


manwordshape = manwords.get_rect()
rabbitwordshape = rabbitwords.get_rect()
dogwordshape = dogwords.get_rect()
queuewordshape = queuewords.get_rect()
noentrywordshape = noentrywords.get_rect()

play = menufont.render("Play", True, red)
instructions = menufont.render("Instructions", True, red)
leave = menufont.render("Quit", True, red)
playshape = play.get_rect()
instructionshape = instructions.get_rect()
leaveshape = leave.get_rect()
greenplay = menufont.render("Play", True, (0, 255, 0))
greeninstructions = menufont.render("Instructions", True, (0, 255, 0))
greenleave = menufont.render("Quit", True, (0, 255, 0))

backbutton = menufont.render("Back", True, red)
greenbackbutton = menufont.render("Back", True, (0, 255, 0))
backbuttonshape = backbutton.get_rect()
backbuttonpos = round_pos((displaysize[0] / 2 - backbuttonshape[2] / 2, 625))
otherbackbuttonpos = round_pos((displaysize[0] / 2 - backbuttonshape[2] / 2, 610))


def round_pos(pos):
    return (round(pos[0]), round(pos[1]))


def showthing(image, words, wordshape, itemsize, randpos):
    screen.blit(image, (hitarea[0] + randpos[0], hitarea[1] + randpos[1]))
    if hitarea[0] - (wordshape[2] / 2) + wordshape[2] > displaysize[0]:
        badlength = 900 - (hitarea[0] + (wordshape[2] / 2))
        screen.blit(
            words,
            (
                hitarea[0] - (wordshape[2] / 2) + badlength,
                manposition[1] + mansize[1] + 5,
            ),
        )
    elif hitarea[0] - (wordshape[2] / 2) < 0:

        badlength = hitarea[0] - (wordshape[2] / 2) - 10

        screen.blit(
            words,
            (
                (hitarea[0] - (wordshape[2] / 2)) - badlength,
                manposition[1] + mansize[1] + 5,
            ),
        )

    else:
        screen.blit(
            words,
            round_pos((hitarea[0] - wordshape[2] / 2, manposition[1] + mansize[1] + 5)),
        )
    if (
        hitarea[0] - mansize[0] < manposition[0] < hitarea[0] + itemsize[0]
        and hitarea[1] - mansize[1] < manposition[1] < hitarea[1] + itemsize[1]
    ):
        return randpos


def randranges(*ranges):
    return random.randint(*random.choice(ranges))


def hitcheck(pos1, size1, pos2, size2):
    if pos2[0] - size1[0] <= pos1[0] <= pos2[0] + size2[0]:
        if pos2[1] - size1[1] <= pos1[1] <= pos2[1] + size2[1]:
            return True
    return False


def hitsidecheck(manpos, mansize, itempos, itemsize):
    global collisionside
    left = manpos[0] + mansize[0] - itempos[0]
    right = itempos[0] + itemsize[0] - manpos[0]
    up = manpos[1] + mansize[1] - itempos[1]
    down = itempos[1] + itemsize[1] - manpos[1]
    if right < left and right < up and right < down:
        collisionside = "right"
    elif left < right and left < up and left < down:
        collisionside = "left"
    elif up < right and up < left and up < down:
        collisionside = "up"
    else:
        collisionside = "down"


initialdeceleration = 0.01
maxdeceleration = 1 * 10 ** -12
deceleration = 0.01
bushcoords = []
toiletcoords = []

bushsize = (25, 25)
toiletsize = (35, 35)
rabbitsize = (30, 20)
angrymansize = (30, 45)
dogsize = (55, 25)
queuesize = (70, 35)
noentrysize = (30, 30)
reliefsize = displaysize
titlesize = (1060, 220)


randpos = None

click = (0, 0, 0)
DT = 1 / 120
hit, showman, showrabbit, showdog, showqueue, shownoentry = (
    False,
    False,
    False,
    False,
    False,
    False,
)

game = False
mainmenu = True
run = True
runinstructions = False
manvelocity = [0, 0]
manposition = [displaysize[0] / 2, displaysize[1] / 2]
speed = 5


bush = loadwithsize("Data/Bush.png", bushsize)
toilet = loadwithsize("Data/Toilet.png", toiletsize)
rabbit = loadwithsize("Data/Rabbit.png", rabbitsize)
angryman = loadwithsize("Data/AngryMan.png", angrymansize)
dog = loadwithsize("Data/Dog.png", dogsize)
queue = loadwithsize("Data/Queue.png", queuesize)
noentry = loadwithsize("Data/NoEntry.png", noentrysize)
relief = loadwithsize("Data/Relief.png", reliefsize)
title = loadwithsize("Data/Title.png", titlesize)

moveup, moveleft, moveright, movedown = False, False, False, False

while run:
    while mainmenu:
        screen.fill((150, 150, 150))
        screen.blit(title, (10, 10))
        screen.blit(dog, (123, 96))
        screen.blit(rabbit, (800, 183))
        screen.blit(angryman, (1050, 675))
        mouse = pygame.mouse.get_pos()
        click = pygame.mouse.get_pressed()
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                mainmenu = False
                run = False

        playpos = round_pos((displaysize[0] / 2 - playshape[2] / 2, 270))
        instructionspos = round_pos((displaysize[0] / 2 - instructionshape[2] / 2, 370))
        leavepos = round_pos((displaysize[0] / 2 - leaveshape[2] / 2, 470))
        if (
            playpos[0] <= mouse[0] <= playpos[0] + playshape[2]
            and 270 <= mouse[1] <= 270 + playshape[3]
        ):
            screen.blit(greenplay, playpos)
            screen.blit(instructions, instructionspos)
            screen.blit(leave, leavepos)

            if click[0] == 1:
                points = 0
                bushcoords = []
                toiletcoords = []
                manposition = [displaysize[0] / 2, displaysize[1] / 2]
                deceleration = initialdeceleration
                for i in range(12):
                    while True:
                        place = (
                            random.randint(0, displaysize[0] - 50 - bushsize[0]),
                            random.randint(0, displaysize[1] - 50 - bushsize[1]),
                        )
                        if not any(
                            hitcheck(place, bushsize, p, bushsize) for p in bushcoords
                        ):
                            break

                    bushcoords.append(place)
                for i in range(6):
                    while True:
                        place = (
                            random.randint(0, displaysize[0] - 50 - toiletsize[0]),
                            random.randint(0, displaysize[1] - 50 - toiletsize[1]),
                        )
                        if not any(
                            hitcheck(
                                place,
                                (toiletsize[0] + 150, toiletsize[1] + 150),
                                p,
                                (toiletsize[0] + 150, toiletsize[1] + 150),
                            )
                            for p in toiletcoords
                        ):
                            break

                    toiletcoords.append(place)

                game = True
                mainmenu = False

        elif (
            instructionspos[0] <= mouse[0] <= instructionspos[0] + instructionshape[2]
            and 370 <= mouse[1] <= 370 + instructionshape[3]
        ):
            screen.blit(play, playpos)
            screen.blit(greeninstructions, instructionspos)
            screen.blit(leave, leavepos)

            if click[0] == 1:
                runinstructions = True
                mainmenu = False

        elif (
            leavepos[0] <= mouse[0] <= leavepos[0] + leaveshape[2]
            and 470 <= mouse[1] <= 470 + leaveshape[3]
        ):
            screen.blit(play, playpos)
            screen.blit(instructions, instructionspos)
            screen.blit(greenleave, leavepos)
            if click[0] == 1:
                run = False
                break

        else:
            screen.blit(play, playpos)
            screen.blit(instructions, instructionspos)
            screen.blit(leave, leavepos)

        pygame.display.flip()

    while runinstructions:
        screen.fill((150, 150, 150))
        for position, instruct in enumerate(instructs):
            screen.blit(instruct, (20, 55 * position + 10))

        mouse = pygame.mouse.get_pos()
        for event in pygame.event.get():
            if event.type in {pygame.MOUSEBUTTONDOWN, pygame.MOUSEBUTTONUP}:
                click = pygame.mouse.get_pressed()
            if event.type == pygame.QUIT:
                runinstructions = False
                run = False
        if (
            backbuttonpos[0] <= mouse[0] <= backbuttonpos[0] + backbuttonshape[2]
            and backbuttonpos[1] <= mouse[1] <= backbuttonpos[1] + backbuttonshape[3]
        ):
            screen.blit(greenbackbutton, backbuttonpos)
            if click[0] == 1:
                mainmenu = True
                runinstructions = False

        else:
            screen.blit(backbutton, backbuttonpos)

        pygame.display.flip()

    while game:
        screen.fill((35, 35, 35))
        clock.tick(120)

        for i in bushcoords:
            screen.blit(bush, i)
        for i in toiletcoords:
            screen.blit(toilet, i)

        for i in bushcoords:
            if not any(
                (showman, showrabbit, showdog, showqueue, shownoentry)
            ) and hitcheck(manposition, mansize, i, bushsize):
                deceleration = maxdeceleration
                hitarea = i
                points += 1
                if random.random() < 0.003 * points:
                    winscreen = True
                    while winscreen:
                        message = myfont.render(
                            "You found a suitable place!", True, (10, 10, 10)
                        )
                        pointswords = myfont.render(
                            "You had a desperation level of: " + str(points),
                            True,
                            (10, 10, 10),
                        )
                        pointswordsshape = pointswords.get_rect()
                        messageshape = message.get_rect()
                        screen.blit(relief, (0, 0))
                        screen.blit(
                            pointswords,
                            (displaysize[0] / 2 - pointswordsshape[2] / 2, 585),
                        )
                        screen.blit(
                            message, (displaysize[0] / 2 - messageshape[2] / 2, 100)
                        )
                        click = pygame.mouse.get_pressed()
                        mouse = pygame.mouse.get_pos()

                        for event in pygame.event.get():
                            if event.type == pygame.QUIT:
                                game = False
                                winscreen = False
                                run = False

                        if (
                            otherbackbuttonpos[0]
                            <= mouse[0]
                            <= otherbackbuttonpos[0] + backbuttonshape[2]
                            and otherbackbuttonpos[1]
                            <= mouse[1]
                            <= otherbackbuttonpos[1] + backbuttonshape[3]
                        ):
                            screen.blit(greenbackbutton, otherbackbuttonpos)
                            if click[0] == 1:
                                mainmenu = True
                                game = False
                                winscreen = False
                        else:
                            screen.blit(backbutton, otherbackbuttonpos)
                        pygame.display.flip()

                else:
                    number = random.randint(0, 2)
                    if number == 0:
                        showman = True
                        while randpos is None or hitcheck(
                            manposition,
                            mansize,
                            (randpos[0] + hitarea[0], randpos[1] + hitarea[1]),
                            angrymansize,
                        ):
                            randpos = (random.randint(-80, 80), random.randint(-80, 80))

                    elif number == 1:
                        showrabbit = True
                        while randpos is None or hitcheck(
                            manposition,
                            mansize,
                            (randpos[0] + hitarea[0], randpos[1] + hitarea[1]),
                            rabbitsize,
                        ):
                            randpos = (random.randint(-20, 20), random.randint(-20, 20))
                    else:
                        showdog = True
                        while randpos is None or hitcheck(
                            manposition,
                            mansize,
                            (randpos[0] + hitarea[0], randpos[1] + hitarea[1]),
                            rabbitsize,
                        ):
                            randpos = (random.randint(-20, 20), random.randint(-20, 20))

        for i in toiletcoords:
            if not any(
                (showman, showrabbit, showdog, showqueue, shownoentry)
            ) and hitcheck(manposition, mansize, i, toiletsize):
                hitsidecheck(manposition, mansize, i, toiletsize)
                hitarea = i
                points += 1
                if random.random() < 0.003 * points:
                    winscreen = True
                    while winscreen:
                        message = myfont.render(
                            "You found a suitable place!", True, (10, 10, 10)
                        )
                        pointswords = myfont.render(
                            "You had a desperation score of: " + str(points),
                            True,
                            (10, 10, 10),
                        )
                        messageshape = message.get_rect()
                        pointswordsshape = pointswords.get_rect()
                        screen.blit(relief, (0, 0))
                        screen.blit(
                            message, (displaysize[0] / 2 - messageshape[2] / 2, 100)
                        )
                        screen.blit(
                            pointswords,
                            round_pos(
                                (displaysize[0] / 2 - pointswordsshape[2] / 2, 585)
                            ),
                        )

                        click = pygame.mouse.get_pressed()
                        mouse = pygame.mouse.get_pos()

                        for event in pygame.event.get():
                            if event.type == pygame.QUIT:
                                game = False
                                winscreen = False
                                run = False

                        if (
                            otherbackbuttonpos[0]
                            <= mouse[0]
                            <= otherbackbuttonpos[0] + backbuttonshape[2]
                            and otherbackbuttonpos[1]
                            <= mouse[1]
                            <= otherbackbuttonpos[1] + backbuttonshape[3]
                        ):
                            screen.blit(greenbackbutton, otherbackbuttonpos)
                            if click[0] == 1:
                                mainmenu = True
                                game = False
                                winscreen = False
                        else:
                            screen.blit(backbutton, otherbackbuttonpos)
                        pygame.display.flip()

                if random.choice((True, False)):
                    shownoentry = True
                    while randpos is None or (
                        hitcheck(
                            manposition,
                            mansize,
                            (randpos[0] + hitarea[0], randpos[1] + hitarea[1]),
                            noentrysize,
                        )
                        or hitcheck(
                            i,
                            toiletsize,
                            (randpos[0] + hitarea[0], randpos[1] + hitarea[1]),
                            noentrysize,
                        )
                    ):
                        randpos = (
                            random.randint(-40, 10 + toiletsize[0]),
                            random.randint(-40, 10 + toiletsize[1]),
                        )

                else:
                    showqueue = True
                    while randpos is None or hitcheck(
                        manposition,
                        mansize,
                        (randpos[0] + hitarea[0], randpos[1] + hitarea[1]),
                        queuesize,
                    ):
                        randpos = (
                            randranges(
                                (-queuesize[0] - 5, -queuesize[0]),
                                (toiletsize[0], toiletsize[0] + 5),
                            ),
                            0,
                        )

        if showman:
            randpos = showthing(angryman, manwords, manwordshape, bushsize, randpos)
            if randpos is None:
                showman = False
                deceleration = initialdeceleration
        if showrabbit:
            randpos = showthing(rabbit, rabbitwords, rabbitwordshape, bushsize, randpos)
            if randpos is None:
                showrabbit = False
                deceleration = initialdeceleration
        if showdog:
            randpos = showthing(dog, dogwords, dogwordshape, bushsize, randpos)
            if randpos is None:
                showdog = False
                deceleration = initialdeceleration

        if showqueue:
            randpos = showthing(queue, queuewords, queuewordshape, toiletsize, randpos)
            if randpos is None:
                showqueue = False
                collisionside = None

        if shownoentry:
            randpos = showthing(
                noentry, noentrywords, noentrywordshape, toiletsize, randpos
            )
            if randpos is None:
                shownoentry = False
                collisionside = None

        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_UP:
                    moveup = True
                if event.key == pygame.K_LEFT:
                    moveleft = True
                if event.key == pygame.K_RIGHT:
                    moveright = True
                if event.key == pygame.K_DOWN:
                    movedown = True
            if event.type == pygame.KEYUP:
                if event.key == pygame.K_UP:
                    moveup = False
                if event.key == pygame.K_LEFT:
                    moveleft = False
                if event.key == pygame.K_RIGHT:
                    moveright = False
                if event.key == pygame.K_DOWN:
                    movedown = False
            if event.type == pygame.QUIT:
                game = False
                run = False

        if moveup:
            manvelocity[1] -= speed * DT
        if moveleft:
            manvelocity[0] -= speed * DT
        if moveright:
            manvelocity[0] += speed * DT
        if movedown:
            manvelocity[1] += speed * DT

        if collisionside == "down" and manvelocity[1] < 0:
            manvelocity[1] = 0
        if collisionside == "up" and manvelocity[1] > 0:
            manvelocity[1] = 0
        if collisionside == "right" and manvelocity[0] < 0:
            manvelocity[0] = 0
        if collisionside == "left" and manvelocity[0] > 0:
            manvelocity[0] = 0

        manvelocity[0] *= deceleration ** DT
        manvelocity[1] *= deceleration ** DT

        if manposition[0] > displaysize[0] - mansize[0]:
            manposition[0] = displaysize[0] - mansize[0]
            manvelocity[0] = 0
        if manposition[0] < 0:
            manposition[0] = 0
            manvelocity[0] = 0
        if manposition[1] > displaysize[1] - mansize[1]:
            manposition[1] = displaysize[1] - mansize[1]
            manvelocity[1] = 0
        if manposition[1] < 0:
            manposition[1] = 0
            manvelocity[1] = 0

        manposition[0] += manvelocity[0]
        manposition[1] += manvelocity[1]
        screen.blit(man, round_pos(manposition))
        pygame.display.flip()
pygame.quit()
